<form class="col s12" method="post" id="id-form-message">
    <div class="row">
        <div class="input-field col s6">
            <input id="first_name" type="text" class="validate">
            <label for="first_name">First Name</label>
        </div>
        <div class="input-field col s6">
            <input id="email" type="email" class="validate">
            <label for="last_name" data-error="wrong" data-success="right">E-Mail</label>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="input-field col s12">
                <textarea id="textarea1" class="materialize-textarea"></textarea>
                <label for="textarea1">Textarea</label>
            </div>
        </div>
    </div>
    <button class="btn waves-effect waves-light" type="submit" name="action">Send
        <i class="material-icons right">send</i>
    </button>
</form>