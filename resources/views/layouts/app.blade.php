<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <!-- Styles -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <nav>
                    <div class="nav-wrapper">
                        <a href="{{ url('/') }}" class="brand-logo center">TestTask</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                        <ul id="dropdown1" class="dropdown-content">
                            <li><a href="{{ url('/logout') }}" role="menu"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                        <ul class="left hide-on-med-and-down">
                            <li><a href="{{ url('/home') }}">Home page</a></li>
                        </ul>
                        <ul class="right hide-on-med-and-down">
                            @if (Auth::guest())
                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Register</a></li>
                            @else
                            <!-- Dropdown Trigger -->
                                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Hi, {{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                            @endif
                        </ul>
                        {{--<ul class="side-nav" id="mobile-demo">
                            @if (Auth::guest())
                                <li><a href="{{ url('/login') }}"><i class="material-icons">input</i>Login</a></li>
                                <li><a href="{{ url('/register') }}"><i class="material-icons">add</i>Register</a></li>
                            @else
                            <!-- Dropdown Trigger -->
                                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Hi, {{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                            @endif
                        </ul>--}}
                    </div>

                </nav>

            </div>

        </div>
    </div>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <!-- Compiled and    minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>--}}
    <script>
           $(".button-collapse").sideNav();
    </script>
    {{--<script src="{{ elixir('js/app.js') }}"></script>--}}
</body>
</html>
