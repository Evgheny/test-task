@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <form role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
                <div class="input-field col s6 offset-s3{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                    <label for="password">Email</label>
                    @if ($errors->has('email'))
                        <div id="card-alert" class="card red lighten-5">
                            <div class="card-content red-text">
                                <p><strong>{{ $errors->first('email') }}</strong></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="input-field col s6 offset-s3{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="validate" name="password">
                    <label for="password">Password</label>
                    @if ($errors->has('password'))
                        <div id="card-alert" class="card red lighten-5">
                            <div class="card-content red-text">
                                <p><strong>{{ $errors->first('password') }}</strong></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="input-field col s6 offset-s3{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input id="password-confirm" type="password" class="validate" name="password_confirmation">
                    <label for="password">Password</label>
                    @if ($errors->has('password_confirmation'))
                        <div id="card-alert" class="card red lighten-5">
                            <div class="card-content red-text">
                                <p><strong>{{ $errors->first('password_confirmation') }}</strong></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col s6 offset-s3">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
