@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="divider"></div>
            <div class="col s6 offset-s3">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <span class="card-title">Logged in home page. </span>
                        <p>You have access on this page only if you are logged in</p>
                    </div>
                    <div class="card-action">
                        @if (Auth::guest())
                            <a href="{{ url('/login') }}"><i class="tiny material-icons">input</i> Login</a>
                            <a href="{{ url('/register') }}"><i class="tiny material-icons">add</i> Register</a>
                        @else
                        <!-- Dropdown Trigger -->
                            <a  href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> LogOut from user {{ Auth::user()->name }}</a></li>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
