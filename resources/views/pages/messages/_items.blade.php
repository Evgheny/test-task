<div class="row">
    @if ( ! $messages->isEmpty() )
        @foreach ($messages as $message)
            <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                <span class="new badge" style="position: absolute; right: 350px;" data-badge-caption="">{{-- 17:15:00 / 03.07.2015 --}}{{ $message->created_at }}</span>
                <li class="active">
                    <div class="collapsible-header active">
                        #{!! $message->id !!}
                        @unless (empty($message->email))
                            <a href="mailto:{{ $message->email }}">{{ $message->name }}</a>
                        @else
                            {{ $message->name }}
                        @endunless
                    </div>
                    <div class="collapsible-body" style="display: block;">
                            <span>{{ $message->message }}</span>
                        <div style="position: relative; height: 70px; margin-bottom: -40px;">
                            <div class="fixed-action-btn horizontal click-to-toggle" style="position: absolute; right: 0;">
                                <a class="btn-floating btn-large red">
                                    <i class="material-icons">settings</i>
                                </a>
                                <ul>
                                    <li><a class="btn-floating red"><i class="material-icons">create</i></a></li>
                                    <li><a class="btn-floating yellow darken-1"><i class="material-icons">delete</i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        @endforeach
    @endif
</div>
<div class="row">
    <div class="col s6 offset-s3" align="center">
        <ul class="pagination">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><a href="#!">1</a></li>
            <li class="waves-effect"><a href="#!">2</a></li>
            <li class="waves-effect"><a href="#!">3</a></li>
            <li class="waves-effect"><a href="#!">4</a></li>
            <li class="waves-effect"><a href="#!">5</a></li>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>
        {{ $messages->render() }}
    </div>
</div>