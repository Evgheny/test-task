@extends('index')

@section('content')

    @include('_common._form')
<hr/>
<div class="row">
    <div class="col s3 offset-s9">
        <div class="collection">
            <a href="#!" class="collection-item">Всего сообщений<span class="new badge"
                                                                      data-badge-caption="">{{ $count }}</span></a>
        </div>
    </div>
</div>
    @include('pages.messages._items')
@stop
