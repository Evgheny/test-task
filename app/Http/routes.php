<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('about', 'HomeController@about');
//Route::get('contact', 'HomeController@contact');
//
//Route::resource('articles', 'ArticlesController');
Route::resource('success', 'SuccessController');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::auth();

Route::get('foo', ['middleware' => 'manager', function(){
  return 'this page may only be viewed by managers :)';
}]);
Route::get('/home', 'HomeController@index');
