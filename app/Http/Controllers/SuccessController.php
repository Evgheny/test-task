<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SuccessController extends Controller
{
    public function index(){
        return view('success.index', compact('success'));
    }
}
